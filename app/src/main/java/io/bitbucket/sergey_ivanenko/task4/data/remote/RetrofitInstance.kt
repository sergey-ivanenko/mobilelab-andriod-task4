package io.bitbucket.sergey_ivanenko.task4.data.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BelarusbankApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val belarusbankApi by lazy {
        retrofit.create(BelarusbankApi::class.java)
    }
}