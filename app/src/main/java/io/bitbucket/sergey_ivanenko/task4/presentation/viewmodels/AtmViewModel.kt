package io.bitbucket.sergey_ivanenko.task4.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.bitbucket.sergey_ivanenko.task4.data.remote.RetrofitInstance
import io.bitbucket.sergey_ivanenko.task4.data.remote.dto.toAtm
import io.bitbucket.sergey_ivanenko.task4.domain.model.Atm
import kotlinx.coroutines.launch

class AtmViewModel : ViewModel() {

    private val _listAtms = MutableLiveData<List<Atm>>()
    val listAtms: LiveData<List<Atm>> get() = _listAtms

    init {
        viewModelScope.launch {
            _listAtms.value = RetrofitInstance.belarusbankApi.getGomelAtms().map { it.toAtm() }
        }
    }
}