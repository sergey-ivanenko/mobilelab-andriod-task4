package io.bitbucket.sergey_ivanenko.task4.data.remote

import io.bitbucket.sergey_ivanenko.task4.data.remote.dto.AtmDto
import retrofit2.http.GET

interface BelarusbankApi {

    @GET("atm/?city=Гомель")
    suspend fun getGomelAtms() : List<AtmDto>

    companion object {
        const val BASE_URL = "https://belarusbank.by/api/"
    }
}