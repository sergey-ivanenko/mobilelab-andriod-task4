package io.bitbucket.sergey_ivanenko.task4.presentation.ui

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ktx.awaitMap
import com.google.maps.android.ktx.awaitMapLoad
import io.bitbucket.sergey_ivanenko.task4.R
import io.bitbucket.sergey_ivanenko.task4.databinding.ActivityMapsBinding
import io.bitbucket.sergey_ivanenko.task4.domain.model.Atm
import io.bitbucket.sergey_ivanenko.task4.presentation.viewmodels.AtmViewModel

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    private val atmViewModel by viewModels<AtmViewModel>()

    private fun getList(): List<Atm> {
        val list: MutableList<Atm> = mutableListOf()
        atmViewModel.listAtms.observe(this, Observer { listOfAtm ->
            listOfAtm.forEach { atm ->
                list.add(atm)
            }
        })
        Log.d("TAG", list.size.toString())
        return list.toList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        lifecycleScope.launchWhenCreated {
            // Get map
            val googleMap = mapFragment.awaitMap()
            addMarkers(googleMap)

            // Wait for map to finish loading
            googleMap.awaitMapLoad()
        }

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        // Add a marker in Gomel and move the camera
        val gomel = LatLng(52.44118, 30.98785)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 10f))
    }

    private fun addMarkers(googleMap: GoogleMap) {
        atmViewModel.listAtms.observe(this, Observer { listOfAtm ->
            listOfAtm.forEach { atm ->
                val marker = googleMap.addMarker(
                    MarkerOptions()
                        .position(atm.latLng)
                )
                marker?.tag = atm
            }
        })
    }
}