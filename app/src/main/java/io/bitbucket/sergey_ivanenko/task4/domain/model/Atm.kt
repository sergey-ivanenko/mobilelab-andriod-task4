package io.bitbucket.sergey_ivanenko.task4.domain.model

import com.google.android.gms.maps.model.LatLng

data class Atm(
    val id: String,
    val area: String,
    val cityType: String,
    val city: String,
    val addressType: String,
    val address: String,
    val house: String,
    val installPlace: String,
    val latLng: LatLng
)

