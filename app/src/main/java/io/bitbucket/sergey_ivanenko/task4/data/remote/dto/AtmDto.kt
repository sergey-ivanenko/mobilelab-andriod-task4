package io.bitbucket.sergey_ivanenko.task4.data.remote.dto

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import io.bitbucket.sergey_ivanenko.task4.domain.model.Atm
import kotlinx.parcelize.Parcelize

@Parcelize
data class AtmDto(
    val id: String,
    val area: String,
    @SerializedName("city_type")
    val cityType: String,
    val city: String,
    @SerializedName("address_type")
    val addressType: String,
    val address: String,
    val house: String,
    @SerializedName("install_place")
    val installPlace: String,
    @SerializedName("gps_x")
    val gpsX: String,
    @SerializedName("gps_y")
    val gpsY: String,
) : Parcelable

fun AtmDto.toAtm(): Atm {
    return Atm(
        id = id,
        area = area,
        cityType = cityType,
        city = city,
        addressType = addressType,
        address = address,
        house = house,
        installPlace = installPlace,
        latLng = LatLng(gpsX.toDouble(), gpsY.toDouble())
    )
}